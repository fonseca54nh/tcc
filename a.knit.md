---
title: Fuzzy control system for an IoT Greenhouse
author: 
- Bruno Gabriel da Fonseca
- Roderval Marcelino
date: 2022, July
output:
  pdf_document:
    includes:
      in_header: preamble.tex
abstract: "Greenhouses are a traditional method of protected cultive, which are experimenting an increased interest for their resilience against harsh weather conditions and better production output. With the advent of new technologies like IoT and artificial intelligence, it is possible to enhance this traditional method and make it smarter. This paper presents a pratical implementation of a fuzzy controller for a smart IoT greenhouse. Internal temperature control is a major variable to be controlled at greenhouses, demanding effort and knowledge from farmers. Thus, an automated system, suited to manage and control these variables can, not only increase production, but also bring ease and reduce the necessary knowledge to maintain a greenhouse. By utilizing a different approach to manage deffuzyfied variables, this controller is able to treat actuators as groups, rather than individuals, which allows it to infer actions, either to cool or heat the greenhouse. Allied with the IoT technology., this solution enables the end user to remotely control and adjust any parameter related to the greenhouse, rendering a complete platform to greenhouse smart control."
bibliography: /home/topcat/ufsc/tcc/fuzzy.bib
---

\renewcommand{\abstractname}{Resumo}
\begin{abstract}
Estufas ou casas de vegetação são um método de cultivo protegido tradicional, que está experimentando um aumento de interesse, devido à sua resiliência a condições climáticas adversas e melhor produtividade. Com o advento de novas tecnologias, como IoT e inteligência artificial, é possível melhorar e torná-las mais inteligentes. Este artigo apresenta uma implementação prática de um controlador fuzzy para uma estufa IoT inteligente. A temperatura interna é uma das variáveis principais a serem controladas em uma estufa, demandando esforço e conhecimento dos agricultores. Portanto, um sistema automatizado, adequado para controlá-la, pode não somente aumentar a produção, mas também trazer facilidade e reduzir o conhecimento necessário para manter uma estufa. Ao utilizar uma abordagem diferente para gerenciar as variáveis deffuzyficadas, este controlador é apto para tratar os atuadores como grupos, ao invés de indivíduos, o que lhe permite inferir ações tanto para resfriar, quanto para esquentar a estufa. Aliada à tecnologia IoT, esta solução habilita o usuário final, para controlar e ajustar cada parâmetro relacionado à estufa remotamente, tornando-se uma plataforma completa para controle inteligente de estufas.
\end{abstract}

\pagebreak

# Introduction

It is not secret that food production is essential for our society. Thus, various agricultural methods exist to enhance production and get better crop yields; one of which greenhouses are a traditional and well-known manner. In recent years however, @zhang2020, smart greenhouses have seen an increasingly high interest but for their energy efficiency and high crop yields.

With the availability of high processing devices, as with recent microcontrollers, complex control systems can be developed to orchestrate multi-variable and multi-output systems. 

The use of IoT is therefore preemptive, as is renders the creation of a sensor network that will serve as a pillar for the intricate greenhouse control system @ali2019. 

Furthermore, in midst of a myriad of control methods, fuzzy controllers, seem to be rather appropriate for the greenhouse thermoclimatic environment @algarin2017, @pacco2022, @amaraillo2018, for their variables are usually bonded together, as with internal temperature, humidity, ventilation, among others, which make them difficult to be mathematically modeled. 

It is possible then, to infer that no simplistic approach would be able to manage such a nebulous system, so a fuzzy algorithm becomes crucial, to correctly output control rules for this correlated microclimatic, as discussed in @zhang2020.

Considering the wireless sensor networks technology, as a possible format for data transferring in such systems, there is a major drawback with greenhouse environments. 

Taking into account humidity and interference, wireless sensor networks may become really unstable in such environment. Thus, for this project, wired sensor networks were chosen, as they provide better stability in such harsh environment.

This paper presents a practical implementation in two greenhouses, constructed in different locations in the south of Brazil. As this project was financed by a private company, two unique structures were built exclusively for this project, one resides on the city of Santa Rosa do Sul, state of Santa Catarina, and the other on the city of Alpestre, state of Rio Grande do Sul, close to the financer's headquarters as requested by them.

It is crucial to perceive, the coexistence of two different crops, bananas and orchids, in these greenhouses. These are tropical plants and require great humidity and high temperatures to develop, hence the importance of a controlled environment is amplified, as the greenhouses are located in a sub-tropical environment which is incompatible with these crops climatic parameters. 

Meanwhile not completely incompatible, crops do not grow adequately during winter times, forcing farmers to buy crops from other warmer states to be able to cultivate them. 

Thus this paper also considers an economic viability; the greenhouse must enable farmers to cultivate multiple tropical crops during any time of the year. Rendering them economic advantage and new market possibilities, where in the past they were practically unable to profit from such crops, as they would hardly thrive without a controlled environment.

To achieve such goal, a composition of different sensors and actuators was required. We were able to measure internal variables: ground moisture, temperature, humidity and luminosity, through the use of humidity, temperature and light sensors. 

And also external ones: temperature, wind speed and direction, pluviometric information, barometric pressure and solar radiation, through the use of a meteorologic station. 

As actuators we have artificial illumination and irrigation, lateral curtains, two covers, windows, two exhausters, a nebulizer, heaters, and a humid wall. By employing these actuators, it is possible, not only to moisture and light the crops, but also directly control the internal temperature and humidity.

This way we were able to cover the most important climatic parameters, a photovoltaic system was also implemented to help reduce energy costs.

Even though the greenhouse covers a wide range of climatic variables, internal temperature was the target variable to be controlled in this paper, and considering no previous database, a fuzzy controller was the best option, for it's simple implementation and good accuracy. 

Humidity, even though having significant importance in crop control @teruel2010, was overlooked during the design of this fuzzy controller, but for the tropical nature of the crops, which demand permanent and stable high levels of humidity throughout time. 

Two growing stages were considered, pre-acclimatization and acclimatization, and thus different climatic conditions arise. For the first stage, humidity needs to be at 95% and temperature needs to float around 25$^{\circ}$C, for 15 days. 

For the second phase, humidity resides at 75%, and temperature floats at 28$^{ \circ }$C. Bananas heavily depend on this parameters, as for orchids are not that demanding.

The fuzzy controller itself needs to only a handful out of all the collected variables to function appropriately. It does need, however, a complete infrastructure, in terms of hardware and software. 

Thus various techniques, that will be further explained ahead, were used to produce a working environment. Distributed computing, IoT, wired sensor networks, were used to create a responsive control software that permits anywhere access to the user. By employing this fuzzy controller, this paper expects to help crops to grow better, stronger and healthier all year long. 

The fuzzy controller presents a novel manner to manage deffuzyfied outputs, as it permits to manage each actuator as a group. By outputting percentages, we were able to control a set of actuators, by activating or deactivating them through the deffuzyfied output percentage. 

Most fuzzy controllers manage the actuators directly by the output as seen in @algarin2017, @pacco2022. However, when considering multiple actuators of the same type, holds back the developer to treat or manage actuators as a group. Thus, we expect to produce a decent controller, even though switching to a new manner of managing the output variables.


# State Of The Art

In the search for relevant scientific production, popular repositories, IEEE Xplore and ScienceDirect, were queried with the following statement: "fuzzy greenhouse".

With IEEE, 191 articles were present, and some were selected by personally reviewing their relation with the topics, 'greenhouse' and 'fuzzy controller'. ScienceDirect articles, on the other hand, presented 9754 results, and were refined by years, 2023 and 2022, resulting in 1297 results, sorted by relevance, and personally review to match the aimed topics.

In @zhang2020, a systematic review of the various control methods was conducted. The authors state the relevance of control methods to improve energy efficiency in greenhouses. Deriving from crescent popularity, greenhouses can mean a proportional increase in production as well energy loss. 

Thus the need, to encounter the most efficient control method; one that is able to deliver a suitable growing environment along minimized energy consumption. 

The authors proceeded with a deep literature review, going through various control methodologies. Fuzzy control method is cited as suitable to control the nonlinear microclimatic environment of greenhouses.

In @ali2019, however, the authors opted to use a recurrent neural network instead of a fuzzy controller, along with a wireless sensor network. Their main goal was to present a simulation model to predict the climatic atmosphere inside a greenhouse. 

A long short-term memory recurrent network was used, and it is important to note the necessity of a previously collected dataset, for training the network. The authors were able to predict the internal temperature, using the proposed neural network controller with high accuracy.

On par with @ali2019, @yaofeng2017, also proposed a modeling method to predict and simulate greenhouse internal climate. A Takagi-Sugeno fuzzy controller however, was used instead of neural network. 

In both cases, good results were obtained, bestowing artificial intelligence with great expectations and confidence towards greenhouse control system usage, as the results were quite accurate.

As with @ali2019 and @yaofeng2017, artificial intelligence is taken with high regards, along with sensors networks, thus these can be further extended by the combined use with IoT technology. 

The newly developed fuzzy controller and simulation models were successful to maintain programmed temperature and humidity, despite severe weather conditions.

@pacco2022, attempted to simulate temperature and control irrigation time with tulip crops in greenhouses at Peru. A fuzzy controller was also used along with a sensor network, communicating however, through the GSM module. 

As pointed by the author, industry and agriculture 4.0, brought many innovations, suggesting the successful combination of IoT and fuzzy controllers, as the proposed fuzzy controller was able, through it's accurate temperature and humidity control, reduce crop loss.

@algarin2017, propose a practical implementation of a web monitored fuzzy controlled miniature greenhouse. An Arduino Mega board was used and along with various sensors and actuators. 

Through the use of the Arduino Ethernet shield and HTML protocol, wireless communication was enabled, thus the main controller could communicate and send data to the webpage.

Considering the miniature greenhouse, the authors were able to emulate different climatic conditions. It was possible then, to verify the fuzzy controller's reaction to broad climatic variances, as it was successful to stabilize key climatic parameters as temperature and humidity. It is important to note, the web interface proposed an easy manner to manage and monitor the greenhouse status.

Not only the importance of greenhouses in food production, but the necessity for automation and self control was observed by @cao2022. The authors discussed the lack of qualified labour to maintain smart greenhouses, thus indicating the need to embrace full automation. 

Denoting advancements in the fields of AI and IoT, automated control systems became fully viable, as the authors proposed an smart agriculture solution based on these technologies. 

Controlling the greenhouse internal thermoclimatic's is key for a functional autonomous greenhouse control. Thus leading the authors to propose an neural network based simulator, that focused on optimizing the climatic strategy for the greenhouse. 

The optimized strategy is put into usage, and as the sensors accumulate data, the strategy is re-optimized accordingly and incrementally.

Looking forward into the tests, the authors concurrently grew tomato crops, with the novel control system, and compared to tomato crops grown by agriculture experts. The proposed solution was able to outperform the experts and boost the production. 

Temperature and humidity were deciding factors, as crops benefiting from the automated control system experienced a more stable and suitable environment than crops managed by agriculture experts.

@madany2017 present a comparison of multiple controller types, including: PI, fuzzy, neural network and neuro-fuzzy controllers. Through the use of simulation tools, the authors were able to simulate a heated greenhouse environment, and compare the controllers performance. 

The authors found that, even though these AI-based controllers are well suited for solving the non linearity problem, the neuro-fuzzy controller was faster and more effective than other controllers.

It is important to denote that most of the present works are, in fact, simulations of a real environment and in some cases practical implementations in minor scale. 

This paper presents a practical implementation in a real scale with two unique structures. Since no previous database exists, a fuzzy controller was chosen instead of a neural one. Also the use of Iot, wired sensor networks and distributed system are present within these structures. 

Hence the importance of this project to the state of the art; as a real scale implementation, we can thoroughly test all these technologies and improve based on collected data, which was previously unavailable.

# Theoretical Background

## Microclimatic variables

As discussed by @chen2011, greenhouse microclimate is nonlinear and composed by multiple correlated factors, like temperature, humidity, light intensity and $CO_{2}$ concentration. Thus, when one variable changes, it directly affects the others.

Even though a greenhouse is a closed environment, it is not only influenced by internal factors, but also by external factors. Therefore, external temperature, humidity and solar radiation, must not be overlooked. 

According to @teruel2010, temperature directly influences plants metabolic functions. Thus, temperature influencing factors like external temperature and ventilation, lighting and solar radiation, must be monitored closely.

Relative humidity @lin2021, @teruel2010, depends on crop transpiration, ventilation and water condensation. Thus it can affect internal heating, plant growth, and is critical to prevent pest and major diseases in crops. Especially in tropical environments, where external humidity is high, ventilation control is critical, as it reduces internal humidity.

Solar radiation and lighting are part of the photosynthesis process. Relative to temperature and humidity, it becomes a considerable variable to be controlled. As pointed out by @teruel2010, @chen2022, @lin2021, not only it affects the irrigation demand, but the whole internal microclimate itself. 

The structure of the greenhouse building, including material and artificial lighting, needs to be thought out considering heat conductibility, as it may boost or decrease solar radiation's impact on the internal climate, as well on the photosynthetic process.

$CO_{2}$, as pointed by @chen2022, plays an important role in photosynthesis. Inside a greenhouse, the crop's own consumption may affect $CO_{2}$ levels by itself. 

It is not to be forgotten, however, that temperature and ventilation can alter these levels as well. Thus by controlling these, we can help maintain $CO_{2}$ levels. Even though a $CO_{2}$ injection may be needed, if low $CO_{2}$ levels are detected.

Ventilation, as seen above, influences both temperature and $CO_{2}$ levels. Therefore, it assists in the cooling process, considering it affects the internal temperature, and helps regulate $CO_{2}$ levels, by passively replenishing the gas inside the greenhouse. 

As these variables are coupled, air flow must be considered when building the greenhouse infrastructure, as well as artificial ventilation methods, like the forced ventilation fans seen in @algarin2017.

## Fuzzy controller

A fuzzy controller is responsible to generate output values for a set of input variables, based off predefined rules. The first step is to convert these input variables to linguistic variables, which is done through the use of membership functions.

These functions heavily depend on the input of experts in the target area @pacco2022. After all data is converted to linguistic variables, inference phase takes place @madany2017, @sabri2012.

For the inference to happen, fuzzy rules need to be defined. These rules will correlate the input variables with an expected output. Usually written in a common manner like: if condition A and condition B the output C, @pacco2022. 

With the rule set defined, the inference phase can happen, where the rules are in fact applied, and an output is generated. The output however is still in a non-algebraic format, and thus needs to be converted. This is called the deffuzification phase, where the output is correctly mapped to a numeric value @sabri2012.

This kind of controller takes advantage over mathematically based controllers, as input data can diverge from the traditional binary approach. With fuzzy controllers, data can be interpolated between 0 and 1 @algarin2017, and the non linearity of systems, and variable coupling is no longer a problem.

Nonlinear systems, as of the greenhouse microclimate, are difficult to be mathematically modeled. As cited by @sabri2012, fuzzy controllers provide a simulated human heuristic, which is much more apt to deal with non linearity. 

Neural networks also perform well on nonlinear systems, fuzzy controllers however, have an advantage, as they do not have the need for a previous database as seen in @iddio2020, @cao2022, @ali2020.

Fuzzy controllers are flexible but for their rule based system. By simply defining linguistic rules, it is possible to correlate input variables to a corresponding output. 

As it is able to interpolate between variables, it can interpret a possible outcome more easily than a traditional mathematical controller, making it well suited for the intricate greenhouse microclimate.

## Internet of Things - IoT

Internet of Things recalls the connection of physical and digital layers through the internet @liu2020. Thus it enables the interaction with the hardware, data access, and remote control trough the internet. 

This leads to the creation of both wired and wireless sensor networks, as all devices and sensors are linked together, through a local network; being interconnected and online, it is possible to remotely access data and control the sensors and actuators.

This technology renders new possibilities in remote tracking and analysis as seen in @amaraillo2018, as WSNs can collect real-time data. Therefore, Internet of Things allied with sensor networks, facilitate the creation of databases, real-time tracking, maintenance and enhance data analysis, specially in remote and difficult access areas, where human interaction may be more difficult.

# Development

On the pursuit of creating a suitable greenhouse environment, it is necessary to design the control system around the most important variables as they are the most concerning and critical factors. 

The greenhouse system considers internal and external temperature, soil humidity, wind speed, solar radiation, precipitation and barometric values, as well irrigation, nebulization and fertigation. 

It is necessary to remark however, that this paper focuses on the fuzzy controller and it was thoroughly built to manage the internal temperature. Even though the scope of this article comprehends just the fuzzy controller, the greenhouse itself was built to be completely automatic and remote controlled.

Taking into account the number of sensors, actuators, variables and physical distance between the two greenhouses, a distributed system is necessary. Thus, for the control system three raspberry pi 4 microcontrollers were used. Two, called slaves, control the sensors and the actuators, and the remaining one, called master, receives their measurements and run the fuzzy controller.

It is also responsible for controlling the user interface that allows an user to manually control the actuators and browse all the greenhouse generated data, that is continuously stored in a MongoDB database.

Benefiting from IoT technology this distributed system culminates in a web application, which enables the user to remotely control the greenhouse and observe it's current state. 

Therefore, by implementing an IoT fuzzy controller, we can remotely manage and adjust all the parameters related to the greenhouse, as they can be manually tuned. Notwithstanding, the fuzzy logic can always be reprogrammed according to the data collected by the sensors network, rendering a complete adaptive system. 

It is important to denote the complexity of hardware and software design for such a extensive system, with many inputs and outputs. Employing a great quantity of sensors and actuators required a distributed system, as it would be impossible for the fuzzy controller to operate well without one.

Hence the necessity to separate stages of development. Tasks were separated in two groups: low level systems and high level end-user application. Two API's were developed separately, each to control a part of the system, one for the low level and the other for the high level elements. 

The fuzzy controller makes use of both API's as it is called by the high level API and utilizes data stored on the database, which is collected by the low level API through the sensors. It then operates on these variables and sends an output to the low level API, that controls the actuators.

Two greenhouses were built at two brazilian cities. One at the State of Santa Catarina, and the other at the State of Rio Grande do Sul. These States do have similar climates, however each city has it's own climate. 

Thus these are no ordinary greenhouses. They were toughly engineered taking into account multiple electronic systems that operate together, and further cooperate with the fuzzy controller.

The image below shows the physical construction of one the greenhouses located at the city of Santa Rosa do Sul.

![Greenhouse Frontal View](assets/estufa1.jpg)

All the actuators, sensors motors, weather station and pretty much any other electric device, operates under the command of the distributed Raspberry system. The image below shows the electronics panel, which resides at the machinery room dedicated to hold all the electronic apparel, as it is better for them to reside outside the greenhouse.

\newpage

![Electronics Panel](assets/painelEletronicaNova.jpg){ width=45% }

## Low-Level systems

The low-level system comprises both the sensors network and the actuators. The sensors and actuators network is further illustrated by the following tables, which contain the model, quantity and type of hardware used.

|             Hardware            |          Model         | Quantity |
|:-------------------------------:+:----------------------:+:--------:|
|         Microcontroller         |  Raspberry Pi Model 4  |     3    |
|         Weather Station         | Davis Advantage Pro II |     1    |
Table: Sensors

|         Hardware         | Quantity |
|:------------------------:+:--------:|
|          Heater          |     4    |
|      Mobile Ceiling      |     1    |
|          Windows         |     2    |
|         Curtains         |     2    |
| Thermoreflecting Ceiling |     1    |
|        Humid Wall        |     1    |
|        Exhaustors        |     1    |
Table: Actuators

Each of these actuators work in a specific way. Curtains have four available positions: fully closed, closed at 33% or 66% and open at 100%; with the purpose of both cooling and heating, depending on the current situation, the greenhouse.

Windows and the ceiling, also aim to control the temperature reducing the greenhouse effect, but work in an open closed format. The thermoreflecting ceiling, also works in a open closed format, however aims to filter solar radiation, as it reflects the heat from the sunlight. These are operated by the motors and regulated by end sensors.

Heaters have the sole purpose of increasing the internal temperature. There are four heaters positioned on each corner of the greenhouse, and they are activated according to the heating percentage that comes from the fuzzy controller output.

On the other hand, the humid wall and the exhaustors operate to cool the greenhouse. It is important to denote that they are always activated together, as they compose the same cooling mechanism, even though they are positioned on opposite walls. 

If the internal temperature ever reaches an extreme value, the fuzzy controller will output a high cooling percentage and the humid wall will certainly be activated. 

The humid wall is made from cork, and as pumps, push water into it and the exhautors are activated, on the other side of the room, outside air is forced into the greenhouse. 

Air passes through the humid wall, and cools the air inside the greenhouse, loosely resembling a radiator. At the figure bellow the humid wall can be seen from the inside, left picture, and from the outside, right picture.

\begin{figure}
\begin{tabular}{ cc }
  \centering
  \includegraphics[width=57mm]{assets/20221205_154535.jpg} & \includegraphics[width=57mm]{assets/20221205_154439.jpg }
\end{tabular}
\end{figure}

Both greenhouses were built using the same hardware, material and structural project. The three Raspberry Pi, compose the center of the distributed system, and are arranged as one Master and two slaves. Both APIs operate on them and are executed on the Master board. 

The weather station is connected directly to the master through an USB port, and the high level API communicates with it through serial protocol. Internal temperature sensors, as well actuators are connected to the slaves boards through GPIO ports, and the low level API communicates with them using the I$^{2}$C protocol.

The low level API was entirely developed using python. It schedules periodic sensor reads at every 10 seconds, discards any non-responding sensor read, and the proceeds to perform an arithmetic mean of the reads, which are then sent to the database, which also resides on the Master board, through a POST request to the High Level API.

The following image shows the distributed Raspberry Pi solution, installed at the greenhouse.

![ Raspberry Distributed System ]( assets/painelRaspNovo2.jpg ){ width=65% }

## High level system

The Master Raspberry Pi hosts the High Level API, the web server and the database. The High Level API is responsible of managing the database, as well the web application, and also communication with the low level API through POST requests. The High level API was developed in Python and JavaScript.

Considering the web application present in this system, important factors as security, data storage and usability, it is necessary to create a suitable software environment to house this structure. 

Thus all the Raspberry Pi boards make use of the Raspian Linux distribution. Thus it was possible to create a containerized environment using Docker, and make use of a non relational database using MongoDB. 

Considering the distance each greenhouse is located and the proposed IoT solution, the creation of a restful API enable the creation of a web application that allow the user to remotely control each greenhouse and it's parameters. 

Thus it is important to note that the web app is responsible for making the integration of low and high levels of the project. On the main screen is possible to observe the current reads from the sensors, as seen on the image below.

![ Raspberry Distributed System ]( assets/frontend.png ){ width=75% }

On the control tab, the user can switch between a manual and automatic control mode, where automatic, means the fuzzy controller will manage the output of the actuators. On the parameters tab, it is possible to set the parameters for the fuzzy controller, for each greenhouse.

After setting the desired parameters and selecting the automatic mode, the fuzzy controller becomes active and will operate to control the internal temperature, based on the selected set point.

## Fuzzy Controller

The fuzzy controller was thoroughly developed to control the internal temperature. It was built using the scikit-learn python library, and take into account three variables: internal and external temperature and wind speed as the antecedents. To achieve a stable internal temperature, the actuators must be activated accordingly to the consequent's output.

Thus the fuzzy controller outputs a heating and a cooling percentages as consequents. Fuzzy controllers in general do not output percentages. However, the nature of the controlled variable, internal temperature, and the actuators, heaters, windows, curtains, ceilings, humid wall and ceilings, call for a different approach. In most traditional fuzzy controllers, fuzzy antecedents are processed into a clearer deffuzyfied variable, as a response.

However, when considering the relation of each actuator with the antecedents, any sort of response from the fuzzy controller would generate another nebulous consequence: how to assign each actuator according to the fuzzy output? To solve this problem, these percentages represent a certain amount of 'cooling' and 'heating' action needed to control the internal temperature. 

Thus it becomes much clearer, to relate each actuator, or a set of actuators, to an output. I.e if the fuzzy controller response calls for 50% cooling and 10% heating, then 30% heaters on. It is possible to infer that, the fuzzy controller outputs a percentage that is further correlated to the actuators activation. 

Hence the novelty of this approach, as it enables the fuzzy controller to manage a set of actuators at once, and prevents the need to control each actuator individually. Thus by treating them as actors, it is possible to assign them 'heating' and 'cooling' actions, enabling them to control the internal temperature by working together.

The use of these percentages allow, also, modifications on the activation of actuators whenever it is needed. If the control system is not performing well, it is possible to adjust the actuators, based on the percentage correlation as demonstrated above.

The fuzzy system uses trapezoidal and triangular member functions to define the antecedent variables universe. This approach is used for the three antecedents. Crops need an acclimatization period and a pre-acclimatization period, with different parameters. 

Thus two different universes were considered accordingly, and for each universe, 75 different rules were set, covering all the possibilities within their range. The inputs are processed by the controller and are further deffuzyfied into the percentages and then correlated with the actuators.

The following code illustrates how this rules were set:

![]( assets/code.png ){ width=50% }

Considering three universes: internal and external temperature and wind speed, a set of linguistic variables was assigned to define each one of them. Internal and external temperature universes for the pre-acclimatization period are present at the image below:

\begin{figure}
\begin{tabular}{ cc }
  \centering
  \includegraphics[width=57mm]{assets/preAclInternalTemperatureUniverse.png} & \includegraphics[width=57mm]{assets/preAclExternalTemperatureUniverse.png}
\end{tabular}
\end{figure}

It is valid to note that this set point is subject to change, depending on the plant to be cultivated. Thus, these universes, pre-acclimatization and acclimatization, were programmed to receive a specified set point for each time the fuzzy controller is called by the High Level API. By doing so, the graphs above representing the set, will change according to the new set point set by the user.

For the pre-acclimatization phase, the ideal temperature set point resides at 25 degrees Celsius. Thus a set called 'Good' was created to indicate, within the fuzzy AI, the target range for the internal temperature. The same occurs for the external temperature, where it matches the same range of the internal one.

For the acclimatization phase however, the internal temperature resides at 28 degrees Celsius. Thus the sets need to be adjusted, as the ideal range moves along the x-axis, as shown in the figure below:

\begin{figure}
\begin{tabular}{ cc }
  \centering
  \includegraphics[width=57mm]{assets/aclInternalTemperatureUniverse.png} & \includegraphics[width=57mm]{assets/aclExternalTemperatureUniverse.png}
\end{tabular}
\end{figure}

The wind speed universe doesn't change for each phase and thus was kept the same for both and is presented as follows:

![Linguistic Variables Membership Universe]( assets/windSpeedUniverse.png ){ width=50% }

The consequents percentages were defined as sets of 10% each, as shown by the image below:

\begin{figure}
\begin{tabular}{ cc }
  \centering
  \includegraphics[width=57mm]{assets/coolingPercentages.png} & \includegraphics[width=57mm]{assets/heatingPercentages.png}
\end{tabular}
\end{figure}

By creating sets, whose range spans only 10%, the fuzzy controller is able to output a percentage from 0 to 100%, without any further complication. And by having two consequents, heating and cooling, the fuzzy controller is able to precisely specify which kind of action, or in other words, which actuator groups, should be activated, completely solving all the microclimate correlation fuzziness.

# Results

This paper seeks to produce a fuzzy controller with the sole purpose of controlling the internal temperature. The internal microclimate of a greenhouse, however, is rather abstract, making it hard to be mathematically modelled. So this fuzzy controller will not be a perfect mathematical solution either. It aims to provide a sound path for reasonable climate control, which does not mean, necessarily, perfect accuracy.

Another factor which influences the results, and surely must be considered, is the actuators accuracy. The machines used as actuators in this project do not allow fine tuning, thus one cannot perfectly adjust them as needed. Accounting that to a fuzzy output, some variation in temperature is expected.

Factors like daytime and season also have a huge impact in the results. Specially the influence of solar radiation, as it will be shown later, the sun's influence makes a big difference in the temperature, and it is even higher in warmer seasons. During the night however, the opposite occurs where, temperature usually has a big drop.

This however is not as bad as it sounds, thanks to the tropical nature of the crops. As most variances occur at night, during daytime, the sun actually helps to maintain temperature at high levels. This means the fuzzy controller will not operate to maintain the temperature perfectly fixed at a set point all the time, but rather operate to diminish major variations in the internal temperature. So it will mostly act in order to prevent huge drops or peaks.

As discussed above, we do not expect the fuzzy controller to perfectly stabilize internal temperature, as such thing is virtually not feasible, considering the actuators used. They would claim for an improvement, if the purpose was to completely stabilize the temperature, which is not a major problem for this study.

Looking forward to analyze the fuzzy controller's performance, it was activated for some days uninterruptedly. The following image shows the performance during twenty four hours of operation, comparing both the internal and external temperature.

![Twenty Four Hour Test Results](assets/alpestre24NOVFull.png)

From the image it is possible to denote what was stated above, where the controller operates to reduce temperature variance. During the night hours, where the external temperature drops, the greenhouse retained temperature at a decent level. On the other hand, during the day, where the external temperature rises, the controller was able to control the internal temperature, preventing an overheat.

# Conclusion

The main goal of this paper was to provide a fuzzy controller solution to control internal temperature inside a greenhouse. Internal temperature, as well as the whole greenhouse microclimate, pose a mathematical difficulty, as it is too challenging to be modelled. Thus a fuzzy controller is ideal to solve such a complex problem. 

Considering other proposed solutions to this problem, this paper presents a real application, where most research is applied to only simulating controllers. It is great then, to further analyze this on the field. The obtained results behaved as expected, considering the intention of this controller was not to stabilize the internal temperature to a fixed set point, but rather stabilize it into a defined range, allowing it to have some variation, whilst preventing major variations, big drops and peaks, to happen.

Considering the tolerance rate, and the results, this fuzzy controller has proven it's value, where it suitable enough to control the internal temperature to acceptable levels during the tests.

There is, of course, room to improvement, as we can further reduce the variation if needed. May the purpose be to fully stabilize internal temperature at a fixed set point, the main limitation of this system will reside at the actuators, as the fuzzy controller is suited to handle variation as shown above. Thus, in order to further reduce it, fine tuned actuators should be used to provide a better response.

# References
