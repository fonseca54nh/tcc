all: $(OBJS)
	pandoc --citeproc header.rmd -o header.pdf
	pandoc --citeproc resumo.rmd -o resumo.pdf
	pandoc --citeproc a.rmd -o a.pdf
	pdfjam header.pdf resumo.pdf a.pdf -o fuzzyControlForAnIoTGreenhouse.pdf

a: $(OBJS)
	pandoc --citeproc a.rmd -o a.pdf
	pdfjam header.pdf resumo.pdf a.pdf -o fuzzyControlForAnIoTGreenhouse.pdf

header: $(OBJS)
	pandoc --citeproc header.rmd -o header.pdf
	pdfjam header.pdf resumo.pdf a.pdf -o fuzzyControlForAnIoTGreenhouse.pdf

resumo: $(OBJS)
	pandoc --citeproc resumo.rmd -o resumo.pdf
	pdfjam header.pdf resumo.pdf a.pdf -o fuzzyControlForAnIoTGreenhouse.pdf
