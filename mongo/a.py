import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt

df = pd.DataFrame( pd.read_csv( "fuzzy.csv" ) ).drop( columns=[ 'id','reliable' ] )

stamp = []

for item in df.timestamp:
    #stamp.append( dt.datetime.fromtimestamp( item ).time().strftime( '%M' ) )
    stamp.append( dt.datetime.fromtimestamp( item ).isoformat() )

df['hour'] = stamp
df['hour'] = df['hour'].astype( 'datetime64' )

#pdf = df.groupby( df.hour ).mean()
pdf = df.set_index("hour")

pdf = pdf.drop( columns=[ 'parent_id', 'timestamp' ] )

print( pdf )

#plt.plot( df.hour, df.value )
plt.style.use("fivethirtyeight")
plt.plot( pdf.value )
plt.show()


