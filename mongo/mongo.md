# Timestamp

Convert a timestamp to a date

```csv 1.csv
date=(timestamp/86400)+DATE(1970,1,1)

86400*( date - date(1970,1,1) ) = timestamp
```

Convert a date to a timestamp

```csv 2.csv
=86400*( date( 2022, 10, 16 ) - date( 1970,1,1 ) )
1665878400
```

( epochconverter.com )[ epochconverter.com ]

# Code

## Mongo

Line to query the database for the internal temperature sorted by highest timestamp

```bash
db.reads.find({\"parent_id\": \"Sensor-Temperatura-1\", \"timestamp\":{$gt: 1663340400,$lt: 1663354800} }).sort({timestamp:-1})
```

Line to dump the query from the database to a csv file

```bash
mongoexport --uri "mongodb://sgh:WQ2YgcFCGwkHdknj@45.188.194.39:27017/" --authenticationDatabase admin --db=sgh --collection=reads --type=csv --fields=id,parent_id,value,reliable,timestamp --query='{"parent_id": "Sensor-Temperatura-1", "timestamp":{"$gt": 1663340400,"$lt": 1663354800} }' --out=data/fuzzy.csv

```

```lua test.lua
require( "sheets" )

local book  = CreateWorkbook( "1.xlsx", "0.00" )
local s2 = CreateSheet( "2.csv" )

s2:Write()

book:Close()

```
