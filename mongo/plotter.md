[tangle]: # ( plotter.py )

## Headers

```python
import pymongo
import pandas as pd
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

```

## DateToTimestamp

Convert a date on the "2022-09-16 12:00:00" format to a unix timestamp

```python
def DateToTimestamp( d ):
	return round( datetime.timestamp( datetime.strptime( d, "%Y-%m-%d %H:%M:%S" ) ) )

```

## Connect to mongo db

Queries the database for a temperature value on the collection reads based on two timestamps, and save a plot to filename

```python
def Plot( firstTime, lastTime, filename ):
	client = pymongo.MongoClient( "mongodb://sgh:WQ2YgcFCGwkHdknj@45.188.194.39:27017/" )
	df = client['sgh']
	collection = df['reads']

	items = collection.find({"parent_id": "Sensor-Temperatura-1", "timestamp":{"$gt": DateToTimestamp( firstTime ),"$lt": DateToTimestamp( lastTime ) } })

	df = pd.DataFrame( items ).drop( columns=[ '_id','reliable' ] )

	stamp = []

	for item in df.timestamp:
	    stamp.append( datetime.fromtimestamp( item ).isoformat() )

	df['hour'] = stamp
	df['hour'] = df['hour'].astype( 'datetime64' )

	pdf = df.set_index("hour")

	pdf = pdf.drop( columns=[ 'parent_id', 'timestamp' ] )

	#plt.plot( df.hour, df.value )
	plt.style.use("fivethirtyeight")
	plt.plot( pdf.value )
	plt.xlabel( "Hora" )
	plt.ylabel( "Temperatura" )
	plt.savefig( filename )
	plt.xticks( rotation=30 )
	plt.show()

```

Call the function

```python
Plot( "2022-09-28 09:00:00", "2022-09-29 09:00:00", "plot.png" )

```
